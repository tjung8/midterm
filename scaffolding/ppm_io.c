//Tony Jung tjung8
//Michael Li mli114

// ppm_io.c
// 601.220, Spring 2019
// Starter code for midterm project - feel free to edit/add to this file

#include <assert.h>
#include "ppm_io.h"
#include <stdlib.h>
#include <stdio.h>


/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {

  // check that fp is not NULL
  assert(fp);
  char a;
  char b;
  if (fscanf(fp, " %c%c", &a, &b) != 2 || a != 'P' || b != '6') {
    return NULL;
  }

  int row;
  int col;
  int color;

  if  (fscanf(fp, " %d ", &col) == 0) {
    char p;
    fscanf(fp, " %c", &p);
    while (p != '\n') {
      fscanf(fp, "%c", &p);
    }
    fscanf(fp, " %d %d %d ", &col, &row, &color);
  }
  else{
    fscanf(fp, " %d %d ", &row, &color);
  }

  Pixel *in = (Pixel *)malloc(sizeof(Pixel) * row * col);

  fread(in, sizeof(Pixel), row * col, fp);
    
  Image *read = (Image *)malloc(sizeof(Image));
  read->data = in;
  read->rows = row;
  read->cols = col;
  
  return read;  //TO DO: replace this stub
}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);

  if (num_pixels_written != im->cols * im->rows) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}

