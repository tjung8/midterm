//Tony Jung tjung8
//Michael Li mli114
#include "ppm_io.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "imageManip.h"
#define PI 3.14159265358979323846264338327
#define sq(x) (x * x)

Image *swap(Image *im) {
  Image *s = (Image *)malloc(sizeof(Image));
  s->rows = im->rows;
  s->cols = im->cols;
  Pixel *n = (Pixel *)malloc(sizeof(Pixel) * im->rows * im->cols);
  s->data = n;
 
  for (int i = 0; i < im->rows * im->cols; i++){
    s->data[i].b = im->data[i].r;
    s->data[i].r = im->data[i].g;
    s->data[i].g = im->data[i].b;
    }
  return s;
}

Image *bright(Image *im, float br) { 
  Image *s = (Image *)malloc(sizeof(Image));
  Pixel *n = (Pixel *)malloc(sizeof(Pixel) * im->rows * im->cols);
  s->data = n;
  for (int i = 0; i < im->rows * im->cols; i++) {
    if (im->data[i].r + br > 255) {
      s->data[i].r = 255;
    }
    else if (im->data[i].r + br < 0) {
      s->data[i].r = 0;
    }
    else {
      s->data[i].r = im->data[i].r + br;
    }
    if (im->data[i].g + br > 255) {
      s->data[i].g = 255;
    }
    else if (im->data[i].g + br < 0) {
      s->data[i].g = 0;
    }
    else{
      s->data[i].g = im->data[i].g + br;
    }
    if (im->data[i].b + br > 255) {
      s->data[i].b = 255;
    }
    else if (im->data[i].b + br < 0) {
      s->data[i].b = 0;
    }
    else {
      s->data[i].b = im->data[i].b + br;
    }
  }
  
  s->cols = im->cols;
  s->rows = im->rows;
  return s;
}


Image *invert(Image *im) {
  Image *s = (Image *)malloc(sizeof(Image));
  Pixel *n = (Pixel *)malloc(sizeof(Pixel) * im->rows * im->cols);
  s->data = n;
  for (int i = 0; i < im->rows * im->cols; i++) {
    s->data[i].r = 255 - im->data[i].r;
    s->data[i].g = 255 - im->data[i].g;
    s->data[i].b = 255 - im->data[i].b;
  }
  s->cols = im->cols;
  s->rows = im->rows;
  return s;
}

Image *gray(Image *im) {
  Image *s = (Image *)malloc(sizeof(Image));
  Pixel *n = (Pixel *)malloc(sizeof(Pixel) * im->rows * im->cols);
  s->data = n;
  double it;
  for (int i = 0; i < im->cols * im->rows; i++) {
    it = 0.30 * im->data[i].r + 0.59 * im->data[i].g + 0.11 * im->data[i].b;
    s->data[i].r = it;
    s->data[i].g = it;
    s->data[i].b = it;
  }
  s->rows = im->rows;
  s->cols = im->cols;
  return s;
}

 Image *crop(Image *im, int topc, int topr, int botc, int botr) {
  Image *s = (Image *)malloc(sizeof(Image));
  s->rows = botr - topr;
  s->cols = botc - topc;
  Pixel *n = (Pixel *)malloc(sizeof(Pixel) * s->rows * s->cols);
  s->data = n;
  
  int ind = 0;
  for (int i = topr; i < botr; i++){
    for (int j = topc; j < botc; j++){
      s->data[ind].r = im->data[i * im->cols + j].r;
      s->data[ind].g = im->data[i * im->cols + j].g;
      s->data[ind].b = im->data[i * im->cols + j].b;
      ind++;
    }
  }
  return s;
 }

Gauss * Gaussian(double sigma){
  int n = ceil(sigma * 10);                                                                                                                                                                               
  if (n % 2 == 0) {                                                                                                                                                                                       
     n += 1;
  }
  Gauss *t = (Gauss *)malloc(sizeof(Gauss) * n * n);                                                                               
  for (int i = 0; i < n; i++){
    for (int j = 0; j < n; j++){
      t[i * n + j].dx = j - (n / 2);
      t[i * n + j].dy = i - (n / 2);
      t[i * n + j].g = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(t[i * n + j].dx) + sq(t[i * n + j].dy)) / (2 * sq(sigma)));
      t[i * n + j].length = 0; //initialize all to 0
    }
  }
  t[0].length = n * n; //holds the length of the gaussian struct
   return t;
}

Pixel filtered(Image* im, int c, int r, Gauss * ga){
  Pixel pix;
  double rcount = 0.0;
  double gcount = 0.0;
  double bcount = 0.0;
  double gausscount = 0.0;
  for (int k = 0; k < ga[0].length; k++){
    if ((c + ga[k].dx >=0) && (r + ga[k].dy >= 0)){ //if in bounds
      rcount += im->data[(r + ga[k].dy) * im->cols + c+ga[k].dx].r * ga[k].g; //multiply and add up r,g,b values                   
      gcount += im->data[(r + ga[k].dy) * im->cols + c+ga[k].dx].g * ga[k].g;
      bcount += im->data[(r + ga[k].dy) * im->cols + c+ga[k].dx].b * ga[k].g;
      gausscount += ga[k].g; //add up gauss values used
    }
  }   
  rcount /= gausscount;
  gcount /= gausscount;
  bcount /= gausscount;//average
  
  pix.r = rcount;//assign to pix
  pix.g = gcount;
  pix.b = bcount;
  return pix;
}

Image *blur(Image *im, double sigma) {
  Image *final = (Image *)malloc(sizeof(Image));
  final->rows = im->rows;
  final->cols = im->cols;
  Pixel *finaldata = (Pixel *)malloc(sizeof(Pixel) * im->rows * im->cols);                                                                                                                                 
  final->data = finaldata;
  Gauss * gs = Gaussian (sigma);
  for (int i = 0; i < im->rows; i++){//change to final->rows
    for (int j = 0; j < im->cols; j++){
      final->data[i * im->cols + j] = filtered(im, j, i, gs);  
    }
  }

  //testing Gaussian generation
  free(gs);
  return final;  
}

Image * edges(Image * im, double sigma, int threshold){
  Image * n = gray(im);
  Image *final = blur(n, sigma);

  free(n->data);
  free(n);
  
  Image finco;
  Pixel *newd = (Pixel *) malloc(sizeof(Pixel) * final->rows * final->cols);
  finco.data = newd;
  for (int i = 0; i < final->rows; i++){
    for (int j = 0; j < final->cols; j++){
      finco.data[i * final->cols + j].r = final->data[i * final->cols + j].r;
      finco.data[i * final->cols + j].g = final->data[i * final->cols + j].g;
      finco.data[i * final->cols + j].b = final->data[i * final->cols + j].b;
    }
  }
  finco.rows = final->rows;
  finco.cols = final->cols;
  
  double intgradx = 0.0;
  double intgrady = 0.0;
  for (int i = 1; i < final->rows - 1; i++){
    for (int j = 1; j < final->cols - 1; j++){
      double inten1 = 0.3 * finco.data[i * final->cols + (j + 1)].r + 0.59 * finco.data[i * final->cols + (j + 1)].g + 0.11 * finco.data[i * final->cols + (j + 1)].b;
      double inten2 = 0.3 * finco.data[i * final->cols + (j - 1)].r + 0.59 * finco.data[i * final->cols + (j - 1)].g + 0.11 * finco.data[i * final->cols + (j - 1)].b;
      double inten3 = 0.3 * finco.data[(i + 1) * final->cols + (j)].r + 0.59 * finco.data[(i + 1) * final->cols + (j)].g + 0.11 * finco.data[(i + 1) * final->cols + (j)].b;
      double inten4 = 0.3 * finco.data[(i - 1) * final->cols + (j)].r + 0.59 * finco.data[(i - 1) * final->cols + (j)].g + 0.11 * finco.data[(i - 1) * final->cols + (j)].b;
      intgradx = (inten1 - inten2) / 2.0;
      intgrady = (inten3 - inten4) / 2.0;
   
      double intgradmag = sqrt(intgradx * intgradx + intgrady * intgrady);
      if(intgradmag > threshold){
	final->data[i * final->cols + j].r = 0;
	final->data[i * final->cols + j].g = 0;
	final->data[i * final->cols + j].b = 0;
      }
      else{
	final->data[i * final->cols + j].r = 255;
        final->data[i * final->cols + j].g = 255;
        final->data[i * final->cols + j].b = 255;
      }
    }
  }
  free(newd);
  return final;
}
