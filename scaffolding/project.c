//Tony Jung tjung8
//Michael Li mli114
#include "ppm_io.h"
#include "imageManip.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
int main(int argc, char *argv[]) {
  FILE* fp = fopen(argv[1], "rb");
  if (fp == NULL) {
    printf("Invalid file\n");
    return 2;
  }

  if(argc < 3) {
    printf("Failed to supply input filename or output filename or both");
    return 1;
  }

  if(argc == 3) {
    printf("No operation name\n");
    return 4;
  }

  Image* r = read_ppm(fp);
  if (r == NULL) {
    printf("Reading input failed\n");
    fclose(fp);
    return 3;
  }
  
  Image* result = NULL;
  
  switch(argc) {
  case 4:
    if (strcmp(argv[3], "swap") == 0) {
      result = swap(r);
      break;
    }
    else if (strcmp(argv[3], "gray") == 0) {
      result = gray(r);
      break;
    }
    else if (strcmp(argv[3], "invert") == 0) {
      result = invert(r);
      break;
    }
    else if (strcmp(argv[3], "blur") == 0 || strcmp(argv[3], "crop") == 0 || strcmp(argv[3], "edges") == 0 || strcmp(argv[3], "bright") == 0){
      printf("Incorrect number of arguments\n");
      free(r->data);
      free(r);
      fclose(fp);
      return 5;
    }
    else {
      printf("Invalid operation\n");
      free(r->data);
      free(r);
      fclose(fp);
      return 4;
    }

  case 5:
    if (strcmp(argv[3], "bright") == 0) {
      result = bright(r, atof(argv[4]));
      break;
    }
    else if (strcmp(argv[3], "blur") == 0) {
      if (atof(argv[4]) < 0) {
        printf("Invalid arguments\n");
        free(r->data);
        free(r);
	fclose(fp);
        return 6;
      }
      result = blur(r, atof(argv[4]));
      break;
    }
    else if (strcmp(argv[3], "swap") == 0 || strcmp(argv[3], "crop") == 0 || strcmp(argv[3], "edges") == 0 || strcmp(argv[3], "gray") == 0 || strcmp(argv[3], "invert") == 0){
      printf("Incorrect number of arguments\n");
      free(r->data);
      free(r);
      free(result);
      fclose(fp);
      return 5;
    }
    else {
      printf("Invalid operation\n");
      free(r->data);
      free(r);
      fclose(fp);
      return 4;
    }
    
  case 6:
    if (strcmp(argv[3], "edges") == 0) {
      if (atof(argv[4]) < 0 || atoi(argv[5]) < 0) {
        printf("Invalid arguments\n");
        free(r->data);
        free(r);
	fclose(fp);
        return 6;
      }
      result = edges(r, atof(argv[4]), atoi(argv[5]));
      break;
    }
    else if (strcmp(argv[3], "swap") == 0 || strcmp(argv[3], "crop") == 0 || strcmp(argv[3], "blur") == 0 || strcmp(argv[3], "gray") == 0 || strcmp(argv[3], "invert") == 0 || strcmp(argv[3], "bright") == 0){
      printf("Incorrect number of arguments\n");
      free(r->data);
      free(r);
      free(result);
      fclose(fp);
      return 5;
    }
    else {
      printf("Invalid operation\n");
      free(r->data);
      free(r);
      fclose(fp);
      return 4;
    }
  case 8:
    if (strcmp(argv[3], "crop") == 0) {
      if (atoi(argv[4]) < 0 || atoi(argv[5]) < 0 || atoi(argv[6]) < 0 || atoi(argv[7]) < 0) {
        printf("Invalid arguments\n");
        free(r->data);
        free(r);
	fclose(fp);
        return 6;
      }
      result = crop(r, atoi(argv[4]), atoi(argv[5]), atoi(argv[6]), atoi(argv[7]));
      break;
    }
    else if (strcmp(argv[3], "swap") == 0 || strcmp(argv[3], "edges") == 0 || strcmp(argv[3], "blur") == 0 || strcmp(argv[3], "gray") == 0 || strcmp(argv[3], "invert") == 0 || strcmp(argv[3], "bright") == \
	     0){
      printf("Incorrect number of arguments\n");
      free(r->data);
      free(r);
      free(result);
      fclose(fp);
      return 5;
    }
    else {
      printf("Invalid operation\n");
      free(r->data);
      free(r);
      fclose(fp);
      return 4;
    }
  default:
    printf("Incorrect number of arguments\n");
    free(r->data);
    free(r);
    fclose(fp);
    return 5;
  }
  
  FILE* w = fopen(argv[2], "wb");
  if (w == NULL) {
    printf("Invalid file\n");
    return 7;
  }
  write_ppm(w, result);
  fclose(fp);
  fclose(w);
  free(r->data);
  free(r);
  free(result->data);
  free(result);
  return 0;
}
