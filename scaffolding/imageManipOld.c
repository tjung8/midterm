#include "ppm_io.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

Image *swap(Image *im) {
  Image *s = (Image *)malloc(sizeof(Image));
  unsigned char red;
  unsigned char green;
  unsigned char blue;
  for (int i = 0; i < im->rows * im->cols; i++){
    red = im->(data + i)->r;
    green = im->(data + i)->g;
    blue = im->(data + i)->b;
    s->(data + i)->r = green;
    s->(data + i)->g = blue;
    s->(data + i) ->b = red;
  }
  s->rows = im->rows;
  s->cols = im->cols;
  return s;
}

Image *bright(Image *im, int br) { 
  Image *s = (Image *)malloc(sizeof(Image));
  for (int i = 0; i < im->rows * im->cols; i++) {
    if (im->(data + i)->r + br > 255) {
      s->(data + i)->r = 255;
    }
    else if (im->(data + i)->r + br < 0) {
      s->(data + i)->r = 0;
    }
    else {
      s->(data + i)->r = im->(data + i)->r + br;
    if (im->(data + i)->g + br > 255) {
      s->(data + i)->g = 255;
    }
    else if (im->(data + i)->g + br < 0) {
      s->(data + i)->g = 0;
    }
    else{
      s->(data + i)->g = im->(data + i)->g + br;
    }
    if (im->(data + i)->b + br > 255) {
      s->(data + i)->b = 255;
    }
    else if (im->(data + i)->b + br < 0) {
      s->(data + i)->b = 0;
    }
    else {
      s->(data + i)->b = im->(data + i)->b + br;
    }
  }
  s->cols = im->cols;
  s->rows = im->rows;
  return s;
}

Image *invert(Image *im) {
  Image *s = (Image *)malloc(sizeof(Image));
  for (int i = 0; i < im->rows * im->cols; i++) {
    s->(data + i)->r = 255 - im->(data + i)->r;
    s->(data + i)->g = 255 - im->(data + i)->g;
    s->(data + i)->b = 255 - im->(data + i)->b;
  }
  s->cols = im->cols;
  s->rows = im->rows;
  return s;
}

Image *gray(Image *im) {
  Image *s = (Image *)malloc(sizeof(Image));
  double it;
  for (int i = 0; i < im->cols * im->rows) {
    it = 0.30 * im->(data + i)->r + 0.59 * im->(data + i)->g + 0.11 * im->(data + i)->b;
    s->(data + i)->r = it;
    s->(data + i)->g = it;
    s->(data + i)->b = it;
  }
  s->rows = im->rows;
  s->cols = im->cols;
  return s;
}

 Image *crop(Image *im, int topc, int topr, int botc, int botr) {
  Image *s = (Image *)malloc(sizeof(Image));
  s->rows = botr - topr;
  s->cols = botc - topc;
  for (int i = (topr - 1) * im->cols + topc; i < (botr - 1) * im->cols + botc; i++) {
    int j = 0;
    s->(data + j)->r = im->(data + i)->r;
    s->(data + j)->g = im->(data + i)->g;
    s->(data + j)->b = im->(data + i)->b;
    j++;
  }
  return s;
 }

 Image *blur(Image *im, double sigma) {
   double n  = ceil(sigma * 10);
   if (n % 2 == 0) {
     n += 1;
   }
   int dx;
   int dy;
   double ga[n][n];
   for (int i = 0; i < n; i++) {
     for (int j = 0; j < n; j++){
       dx = i - n / 2;
       dy = j - n / 2;
       ga[i][j] = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));
     }
   }

   Pixel p[im->rows][im->cols]; 
   int ro;
   int co;
   for (int k = 0; k < im->rows * im->cols; k++) {
     ro = k / im->cols;
     co = k % im->cols;
     p[ro][co] = im->*(data+k);
   }

   
 }
