//Tony Jung tjung8
//Michael Li mli114
#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

#include <stdio.h>
#include <math.h>

typedef struct Gauss {
  int dx;
  int dy;
  double g;
  int length;
} Gauss;

Image *swap(Image *im);

Image *bright(Image *im, float br);

Image *invert(Image *im);

Image *gray(Image *im);

Image *crop(Image *im, int topc, int topr, int botc, int botr);

Gauss * Gaussian(double sigma);

Pixel filtered(Image* im, int c, int r, Gauss * ga);

Image * blur(Image *im, double sigma);

Image * edges(Image * im, double sigma, int threshold);
#endif
